import React from 'react';
import Select, { ValueType } from 'react-select';
import { useField } from 'formik';

import { SelectOption } from '../../../models/SelectOption';
import { FORMAT_OPTIONS } from '../../../utils/FORMAT_OPTIONS'

interface FormatFacetInterface {
  name: string;
}

export const FormatFacet: React.SFC<FormatFacetInterface> = ({ name }) => {
  const [field, , helpers] = useField({ name });
  const onFormatsChange = (option: ValueType<SelectOption | SelectOption[]>) => {
    const val = option ? (option as SelectOption[]).map((item: SelectOption) => item.value) : [];
    helpers.setValue(val);
  };

  return <Select
    isMulti
    value={FORMAT_OPTIONS.filter((option) => field.value.includes(option.value))}
    options={FORMAT_OPTIONS}
    onChange={onFormatsChange}
  />;
};
