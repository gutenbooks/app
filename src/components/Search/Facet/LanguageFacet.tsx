import React, { useState, useEffect } from 'react';
import Select, { ValueType } from 'react-select';
import { useField } from 'formik';

import { Language } from '../../../models/Language';
import { SelectOption } from '../../../models/SelectOption';

interface LanguageFacetInterface {
  name: string;
  languages: Language[];
}

const mapOptions = (languages: Language[]) => {
  return languages.map((lang) => ({ value: lang.code, label: lang.name } as SelectOption))
};

export const LanguageFacet: React.SFC<LanguageFacetInterface> = ({ name, languages }) => {
  const [options, setOptions] = useState<SelectOption[]>(mapOptions(languages));

  useEffect(() => {
    setOptions(mapOptions(languages))
  }, [languages]);

  const [field, , helpers] = useField({ name });
  const onLanguageChange = (option: ValueType<SelectOption>) => {
    helpers.setValue(option ? (option as SelectOption).value : options[0].value);
  };

  return <Select
    value={field.value ? options.filter((option) => field.value === option.value) : options[0]}
    options={options}
    onChange={onLanguageChange}
  />;
};
