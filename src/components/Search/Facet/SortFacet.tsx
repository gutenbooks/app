import React from 'react';
import Select, { ValueType } from 'react-select';
import { useField } from 'formik';

import { SelectOption } from '../../../models/SelectOption';
import { SORT_OPTIONS } from '../../../utils/SORT_OPTIONS';

interface SortFacetInterface {
  name: string;
}

const getValue = (field: any): SelectOption|undefined => {
  if (field && field.value) {
    return SORT_OPTIONS.find((option) => {
      return field.value.field === option.value.field &&
        field.value.order === option.value.order;
    });
  }

  return SORT_OPTIONS[0];
}

export const SortFacet: React.SFC<SortFacetInterface> = ({ name }) => {
  const [field, , helpers] = useField({ name });
  const onSortChange = (option: ValueType<SelectOption>) => {
    helpers.setValue(option ? (option as SelectOption).value : SORT_OPTIONS[0].value);
  };

  return <Select
    value={getValue(field)}
    options={SORT_OPTIONS}
    onChange={onSortChange}
  />;
};
