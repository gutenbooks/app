import React from 'react';
import { Form } from 'react-bootstrap';

import { Taxonomy } from '../../models/Taxonomy';
import { Language } from '../../models/Language';
import { SearchTaxonFilter } from './SearchTaxonFilter';
import { FormatFacet } from './Facet/FormatFacet';
import { LanguageFacet } from './Facet/LanguageFacet';
import { SortFacet } from './Facet/SortFacet';

interface SearchFacetsProps {
  values: any;
  taxonomies: Taxonomy[];
  languages: Language[];
}

export const SearchFacets: React.SFC<SearchFacetsProps> = ({
  values,
  taxonomies,
  languages,
}) => {

  return (
    <div>
      <h6>Advanced Search Options</h6>
      <Form.Group controlId="searchFacets.sort">
        <Form.Label>Sort</Form.Label>
        <SortFacet name="sort" />
      </Form.Group>

      <Form.Group controlId="searchFacets.language">
        <Form.Label>Language</Form.Label>
        <LanguageFacet name="language" languages={languages} />
      </Form.Group>

      <Form.Group controlId="searchFacets.formats">
        <Form.Label>Formats</Form.Label>
        <FormatFacet name="formats"/>
      </Form.Group>

      <SearchTaxonFilter
        // temporarily disable due to performances issues as the result
        // of a massive and flat taxon/taxonomy structure.
        // taxonomies={taxonomies}
        taxonomies={[]}
        values={values}
      />
    </div>
  );
};
