import React, { useState } from 'react';
import { Formik } from 'formik';
import { Collapse, Card, InputGroup, FormControl, Form, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faSlidersH } from '@fortawesome/free-solid-svg-icons'

import { useLanguages } from '../../hooks/useLanguages';
import { useTaxonomies } from '../../hooks/useTaxonomies';
import { OnSearchCallback } from '../../types/OnSearchCallback';
import { SearchFormInterface } from '../../models/SearchFormInterface'
import { SearchFacets } from './SearchFacets';

interface SearchControlInterface {
  defaultValues?: SearchFormInterface;
  onSearch: OnSearchCallback;
  placeholder?: string;
}

const defaultForm: SearchFormInterface = {
  query: '',
  taxons: [],
  formats: [],
  language: 'en',
};

export const SearchControl: React.SFC<SearchControlInterface> = ({
  defaultValues = defaultForm,
  onSearch,
  placeholder = 'Search (Title, Subtitle, Author, etc.)'
}) => {
  const [open, setOpen] = useState(false);
  const languages = useLanguages();
  const taxonomies = useTaxonomies();

  return (
    <Formik
      onSubmit={(values) => {
        onSearch(values)
      }}
      initialValues={defaultValues}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        setFieldValue,
        values,
        touched,
        isValid,
        errors,
      }) => (
        <Card className="mb-3">
          <Form onSubmit={handleSubmit}>
            <InputGroup>
              <InputGroup.Prepend>
                <Button
                  variant="outline-primary"
                  onClick={() => setOpen(!open)}
                  aria-expanded={open}
                  active={open}
                >
                  <FontAwesomeIcon icon={faSlidersH} />
                </Button>
              </InputGroup.Prepend>
              <FormControl
                name="query"
                value={values.query}
                onChange={handleChange}
                placeholder={placeholder}
                aria-label={placeholder}
                aria-describedby="basic-addon2"
              />
              <InputGroup.Append>
                <Button
                  variant="primary"
                  type="submit"
                >
                  <FontAwesomeIcon icon={faSearch} />
                </Button>
              </InputGroup.Append>
            </InputGroup>

            <Collapse in={open}>
              <Card.Body>
               <SearchFacets
                values={values}
                taxonomies={taxonomies}
                languages={languages}
               />
             </Card.Body>
            </Collapse>
          </Form>
        </Card>
      )}
    </Formik>
  );
};
