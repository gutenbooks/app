import React from 'react';
import { Helmet } from 'react-helmet';

export interface MetaProps {
  description?: string;
  lang?: string;
  meta?: any[];
  keywords?: string[];
  title?: string;
}

export const Meta: React.FC<MetaProps> = ({
  description = `A better Gutenberg.org`,
  lang = `en`,
  meta = [],
  keywords = [],
  title = ``,
}) => {

  const standardMeta = [
    {
      name: `description`,
      content: description,
    },
    {
      property: `og:title`,
      content: title,
    },
    {
      property: `og:description`,
      content: description,
    },
    {
      property: `og:type`,
      content: `website`,
    },
    {
      name: `twitter:card`,
      content: `summary`,
    },
    {
      name: `twitter:title`,
      content: title,
    },
    {
      name: `twitter:description`,
      content: description,
    },
  ];

  let keywordsMeta: any = [];
  if (keywords && keywords.length > 0) {
    keywordsMeta = {
      name: `keywords`,
      content: keywords.join(`, `),
    };
  }

  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={title}
      titleTemplate={`%s // Gutenbooks`}
      meta={
        standardMeta
          .concat(keywordsMeta)
          .concat(meta)
      }
    />
  );
};
