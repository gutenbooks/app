import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList } from '@fortawesome/free-solid-svg-icons';

import { Book } from '../../models/Book';
import { ClickableBook } from '../../types/ClickableBook';
import { ClickableContributor } from '../../types/ClickableContributor';
import { BookItem } from './BookItem';

interface BookItemListProps {
  books: Book[];
  onClickBook: ClickableBook;
  onClickAuthor: ClickableContributor;
}

export const BookItemList: React.SFC<BookItemListProps> = ({ books, onClickBook, onClickAuthor }: BookItemListProps) => {

  if (books.length === 0) {
    return (
      <Row>
        <Col className="text-center">
          <FontAwesomeIcon icon={faList} />
          <p className="text-muted small">no items in list</p>
        </Col>
      </Row>
    );
  }

  return (
    <Row className="row-cols-1 row-cols-sm-2 row-cols-md-4">
      {
        books.map((book: Book) => {
          return (
            <Col
              key={`book-item-${book.id}`}
            >
              <BookItem
                book={book}
                onClickBook={onClickBook}
                onClickAuthor={onClickAuthor}
              />
            </Col>
          );
        })
      }
    </Row>
  );
};
