import React, { useState } from 'react';
import { Form, Row, Col } from 'react-bootstrap';

import { BookAttributes } from './BookAttributes';
import { BookFormats } from './BookFormats';
import { BookImage } from './BookImage';
import { BookRating } from './BookRating';
import { Book } from '../../models/Book';
import { ContributorList } from '../Contributor/ContributorList';

interface BookShowInterface {
  book: Book;
}

export const BookShow: React.SFC<BookShowInterface> = ({ book }: BookShowInterface) => {
  const [edition, setEdition] = useState(book.editions[0]);

  return (
    <Row>
      <Col md={3}>
        <BookImage book={book} edition={edition} />
      </Col>
      <Col md={6}>
        <h1 className="mt-2">{book.title}</h1>
        <h3 className="mt-2">{book.subtitle}</h3>
        <ContributorList contributions={book.contributions} />
        <BookRating rating={book.rating} />
        <div className="mb-3">
          {
            book.editions.map((e, idx) => {
              return (
                <Form.Check
                  key={`edition-${idx}`}
                  type="radio"
                  label={`Edition #${idx + 1}${e.subtitle !== book.subtitle ? ` - ${e.subtitle}` : ''}`}
                  value={idx}
                  checked={e.id === edition.id}
                  onChange={(e: any) => setEdition(book.editions[e.currentTarget.value])}
                />
              );
            })
          }
        </div>

        <BookFormats
          id={book.id}
          title={book.title}
          formats={edition.formats}
        />
      </Col>
      <Col md={3}>
        <BookAttributes
          book={book}
          edition={edition}
        />
      </Col>
      <Col xs={12} className="mt-4">
        {
          book.description ?
            <p dangerouslySetInnerHTML={{ __html: book.description }}></p>
          :
            null
        }
      </Col>
    </Row>
  );
};
