import React from 'react';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import ReactGA from 'react-ga';

import { Format } from '../../models/Format';
import { FormatType } from '../../models/FormatType';

interface BookImageProps {
  id: number|string;
  title: string;
  formats: Format[];
}

const FormatTypeDisplay = {
  [FormatType.EPUB]: 'EPUB',
  [FormatType.KINDLE]: 'Kindle',
  [FormatType.PLAIN_TEXT]: 'Text',
  [FormatType.HTML]: 'HTML',
  [FormatType.PDF]: 'PDF',
}

export const BookFormats: React.SFC<BookImageProps> = ({ id, title, formats = [] }: BookImageProps) => {
  const propId = `book-formats-${id}`;

  const onClick = (action: string) => {
    return (e: any) => {
      ReactGA.event({
        category: 'Download',
        action,
        label: title,
      });
    }
  }

  return (
    <DropdownButton
      alignRight
      title="Download"
      drop='up'
      id={propId}
    >
      {
        formats.map((format: Format, idx: number) => {
          return (
            <Dropdown.Item
              key={`${propId}-${idx}`}
              target="_blank"
              href={format.file}
              eventKey={`${idx}`}
              onClick={onClick(format.type)}
            >
              {FormatTypeDisplay[format.type]} { format.description ? `(${format.description})` : '' }
            </Dropdown.Item>
          );
        })
      }
    </DropdownButton>
  );
};
