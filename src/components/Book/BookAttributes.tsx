import React from 'react';
import { Card, Table } from 'react-bootstrap';
import moment from 'moment';

import { Book } from '../../models/Book';
import { Edition } from '../../models/Edition';
import { Identifier, IdentifierType, IdentifierSource } from '../../models/Identifier';

interface BookAttributesInterface {
  book: Book;
  edition: Edition;
}

const IdentifierSourceDisplay = {
  [IdentifierSource.GUTENBERG]: 'Project Gutenberg ID',
  [IdentifierSource.GOODREADS]: 'Goodreads ID',
  [IdentifierSource.GLOBAL]: 'Global ID',
}

const identifierMapper = (isBook: boolean = false) => (identifier: Identifier) => {
  let display: any = identifier.type.toUpperCase();
  let value: any = identifier.value;

  if (identifier.type === IdentifierType.INTERNAL) {
    display = IdentifierSourceDisplay[identifier.source];
  }

  if (identifier.source === IdentifierSource.GUTENBERG) {
    value = (
      <a
        target="_blank"
        rel="noopener noreferrer"
        href={`http://www.gutenberg.org/ebooks/${identifier.value}`}
      >
        {identifier.value}
      </a>
    );
  } if (identifier.source === IdentifierSource.GOODREADS && identifier.type === IdentifierType.INTERNAL) {
    value = (
      <a
        target="_blank"
        rel="noopener noreferrer"
        href={`https://www.goodreads.com/${ isBook ? 'work/editions' : 'book/show' }/${identifier.value}`}
      >
        {identifier.value}
      </a>
    );
  }

  return (
    <tr>
      <td><strong>{display}</strong></td>
      <td>{value}</td>
    </tr>
  );
};

export const BookAttributes: React.SFC<BookAttributesInterface> = ({ book, edition }: BookAttributesInterface) => {
  return (
    <Card>
      <Card.Body>
        <Table responsive hover className="small">
          <tbody>
            <tr>
              <td style={{ borderTop: 'none' }}><strong>Published</strong></td>
              <td style={{ borderTop: 'none' }}>{moment(edition.publishedAt).format('MMMM Do YYYY')}</td>
            </tr>
            <tr>
              <td><strong>Downloads (Total)</strong></td>
              <td>
                {
                  book.editions.reduce((acc, edition: any) => {
                    return acc + edition.downloads
                  }, 0)
                }
              </td>
            </tr>

            <tr>
              <td><strong>Downloads (Edition)</strong></td>
              <td>{edition.downloads}</td>
            </tr>

            {
              book.identifiers.map(identifierMapper(true), true)
            }

            {
              edition.identifiers.map(identifierMapper())
            }
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  );
};
