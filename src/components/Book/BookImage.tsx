import React from 'react';
import styled from 'styled-components';

import { Book } from '../../models/Book';
import { Edition } from '../../models/Edition';
import { Identifier } from '../../models/Identifier';

// bound all images to 200x300px
const ImageBoundingBox = styled.div`
  width: 200px;
  height: 300px;
  margin: auto;
`;

export enum Size {
  Small = 'small',
  Medium = 'medium',
  Large = 'large',
}

interface BookImageProps {
  book: Book;
  edition?: Edition;
  size?: Size;
}

const getIdentifier = (book: Book, edition?: Edition): Identifier|null => {
  const e = edition || book.editions[0];
  for (const identifier of e.identifiers) {
    if (identifier.source === 'project_gutenberg' && identifier.type === 'internal') {
      return identifier;
    }
  }

  return null;
}

export const BookImage: React.SFC<BookImageProps> = ({ book, edition, size=Size.Medium }: BookImageProps) => {
  const placeholder = `https://via.placeholder.com/200x300`;
  const onError = (e: any) => {
    e.target.src = placeholder;
  };

  const identifier = getIdentifier(book, edition);

  if (!identifier) {
    return (
      <ImageBoundingBox>
        <img
          src={placeholder}
          alt={book.title}
          onError={onError}
        />
      </ImageBoundingBox>
    )
  }

  const image = `https://www.gutenberg.org/cache/epub/` +
    `${identifier.value}/pg${identifier.value}.cover.${size}.jpg`;

  return (
    <ImageBoundingBox>
      <img
        src={image}
        alt={book.title}
        onError={onError}
      />
    </ImageBoundingBox>
  );
};
