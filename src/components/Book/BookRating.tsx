import React from 'react';
import Rating from 'react-rating';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarOpen } from '@fortawesome/free-regular-svg-icons';

interface BookRatingProps {
  rating: number|null;
}

export const BookRating: React.FC<BookRatingProps> = ({ rating }) => {

  if (rating === null) {
    return <><br/><br/></>;
  }

  return <Rating
    initialRating={rating}
    emptySymbol={<FontAwesomeIcon icon={faStarOpen} color="gold" />}
    fullSymbol={<FontAwesomeIcon icon={faStar} color="gold" />}
    fractions={2}
    readonly
  />;
};
