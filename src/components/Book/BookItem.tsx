import React from 'react';

import { BookImage } from './BookImage';
import { BookFormats } from './BookFormats';
import { BookRating } from './BookRating';
import { Book } from '../../models/Book';
import { ContributorList } from '../Contributor/ContributorList';
import { ClickableBook } from '../../types/ClickableBook';
import { ClickableContributor } from '../../types/ClickableContributor';

interface BookItemInterface {
  book: Book;
  onClickBook: ClickableBook;
  onClickAuthor: ClickableContributor;
}

export const BookItem: React.SFC<BookItemInterface> = ({ book, onClickBook, onClickAuthor }: BookItemInterface) => {
  return (
    <div className="text-center mb-3">
      <button className="btn btn-link" onClick={() => onClickBook(book)}>
        <BookImage book={book} />
        <h6 className="mt-2 d-block text-truncate" style={{maxWidth: '15rem'}}>
          {book.title}
        </h6>
        <h6 className="mt-2 d-block text-truncate small" style={{maxWidth: '15rem'}}>
          {book.subtitle ? book.subtitle : (<>&nbsp;</>) }
        </h6>
      </button>
      <BookRating rating={book.rating} />
      <ContributorList contributions={book.contributions} />
      <BookFormats id={book.id} title={book.title} formats={book.editions[0].formats} />
    </div>
  );
}
