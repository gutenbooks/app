import React, { useState, useEffect } from 'react';

import { ApiClient } from '../utils/ApiClient';
import { useContributorSelectionHandler } from '../hooks/useContributorSelectionHandler';
import { useBookSelectionHandler } from '../hooks/useBookSelectionHandler';
import { BookCarousel } from '../components/Book/BookCarousel';
import { Loader } from '../components/Loader/Loader';

export const PopularBooks: React.FC = () => {
  const [popular, setPopular] = useState([]);
  const [loading, setLoading] = useState(false);
  const onClickAuthor = useContributorSelectionHandler();
  const onClickBook = useBookSelectionHandler();

  useEffect(() => {
    setLoading(true)
    ApiClient.get('/books/')
      .then(({data: { data }}) => {
        setPopular(data);
        setLoading(false);
      })
    ;
  }, []);

  return (
    <Loader isLoading={loading}>
      <BookCarousel
        title="Popular"
        books={popular}
        onClickBook={onClickBook}
        onClickAuthor={onClickAuthor}
      />
    </Loader>
  );
};
