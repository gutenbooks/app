import React, { useState, useEffect } from 'react';
import { RequestQueryBuilder } from '@nestjsx/crud-request';

import { ApiClient } from '../utils/ApiClient';
import { useContributorSelectionHandler } from '../hooks/useContributorSelectionHandler';
import { useBookSelectionHandler } from '../hooks/useBookSelectionHandler';
import { BookCarousel } from '../components/Book/BookCarousel';
import { Loader } from '../components/Loader/Loader';

export const RecentBooks: React.FC = () => {
  const [books, setBooks] = useState([]);
  const [loading, setLoading] = useState(false);
  const onClickAuthor = useContributorSelectionHandler();
  const onClickBook = useBookSelectionHandler();

  useEffect(() => {
    setLoading(true);

    const q = RequestQueryBuilder
      .create()
      .setPage(1)
      .sortBy({ field: 'createdAt', order: 'DESC' })
      .query()
    ;

    ApiClient.get(`/books/?${q}`)
      .then(({data: { data }}) => {
        setBooks(data);
        setLoading(false);
      })
    ;
  }, []);

  return (
    <Loader isLoading={loading}>
      <BookCarousel
        title="Recently Added"
        books={books}
        onClickBook={onClickBook}
        onClickAuthor={onClickAuthor}
      />
    </Loader>
  );
};
