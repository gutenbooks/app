import React from 'react';

import { useContributorSelectionHandler } from '../../hooks/useContributorSelectionHandler';
import { Contribution } from '../../models/Contribution';
import { ContributionType } from '../../models/ContributionType';

interface ContributorListProps {
  contributions?: Contribution[];
  type?: ContributionType;
}

export const ContributorList: React.FC<ContributorListProps> = ({
  contributions = [],
  type = ContributionType.Author,
}) => {

  const onClick = useContributorSelectionHandler();
  const contribs = contributions.filter((contribution) => {
    return contribution.type === type;
  });

  if (contribs.length === 0) {
    return (
      <p className="small text-center">
        ---
      </p>
    );
  }

  return (
    <p className="small">
      By {
        contribs.map((contribution, i) => {
          const contributor = contribution.contributor;
          return (
            <span key={contributor.name}>
              <button
                className="btn btn-link"
                onClick={() => onClick(contributor)}
              >
                {contributor.name}
              </button>{ i < contribs.length - 1? ', ' : '' }
            </span>
          )
        })
      }
    </p>
  );
}
