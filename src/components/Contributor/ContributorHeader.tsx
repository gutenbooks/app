import React, { useState, useEffect } from 'react';
import { Jumbotron } from 'react-bootstrap';

import { ApiClient } from '../../utils/ApiClient';
import { Contributor } from '../../models/Contributor';

interface ContributorHeaderProps {
  id: string|number;
}

export const ContributorHeader: React.FC<ContributorHeaderProps> = ({ id }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<any>();
  const [contributor, setContributor] = useState<Contributor>();

  useEffect(() => {
    setIsLoading(true);
    ApiClient.get(`/contributors/${id}`)
      .then(({data}) => {
        setIsLoading(false);
        setContributor(data);
      })
      .catch((err) => {
        setError(() => err);
      })
    ;
  }, [id]);

  if (isLoading) {
    return (<p>loading...</p>);
  } else if (error) {
    return (
      <p>error loading contributor {id}</p>
    );
  }

  return (
    <Jumbotron className="text-center">
      <h1>{contributor ? contributor.name : null}</h1>
      <div className="row justify-content-center">
        <div className="col-6 col-md-2">
          <hr/>
        </div>
      </div>
      <p className="small text-muted">Filter & Search within the contributors works.</p>
    </Jumbotron>
  );
}
