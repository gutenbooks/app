import React from 'react';

export const Brand: React.FC = () => {
  return (
    <>
      <span className="font-weight-light">guten</span><span className="font-weight-bold">books</span>
    </>
  );
}
