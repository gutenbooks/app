
import { Contribution } from './Contribution';
import { Edition } from './Edition';
import { Identifier } from './Identifier';

export interface Book {
  id: string;
  title: string;
  subtitle: string|null;
  description: string;
  rating: number;
  createdAt: string;
  updatedAt: string;
  identifiers: Identifier[];
  editions: Edition[];
  contributions: Contribution[];
}
