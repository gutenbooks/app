export enum FormatType {
  EPUB = 'epub',
  KINDLE = 'kindle',
  PLAIN_TEXT = 'text',
  HTML = 'html',
  PDF = 'pdf',
};
