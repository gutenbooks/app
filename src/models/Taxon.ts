export interface Taxon {
  id: number;
  name: string;
  parent: Taxon;
  children: Taxon[];
}
