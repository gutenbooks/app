import { FormatType } from './FormatType';

export interface SearchFormInterface {
  query: string;
  language: string;
  formats: FormatType[];
  sort?: any;
  taxons?: string[];
  page?: number;
}
