
import { ContributionType } from './ContributionType';
import { Contributor } from './Contributor';

export interface Contribution {
  type: ContributionType;
  contributor: Contributor;
}
