export enum IdentifierSource {
    GUTENBERG = 'project_gutenberg',
    GOODREADS = 'goodreads',
    GLOBAL = 'global',
}

export enum IdentifierType {
    ISBN = 'isbn',
    ISBN13 = 'isbn13',
    INTERNAL = 'internal',
}

export interface Identifier {
  id: string;
  source: IdentifierSource;
  type: IdentifierType;
  value: string;
  editionId: string;
  createdAt: string;
  updatedAt: string;
}
