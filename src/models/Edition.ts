import { Format } from './Format';
import { Identifier } from './Identifier';

export interface Edition {
  id: string;
  title: string;
  subtitle: null,
  downloads: number,
  publishedAt: string;
  createdAt: string;
  updatedAt: string;
  formats: Format[];
  identifiers: Identifier[];
}
