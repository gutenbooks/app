export interface Contributor {
  id: number;
  name: string;
  sortName: string;
}
