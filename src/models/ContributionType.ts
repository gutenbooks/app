export enum ContributionType {
  Author = 'author',
  Editor = 'editor',
  Illustrator = 'illustrator',
}
