import { FormatType } from './FormatType';

export interface Format {
  file: string;
  type: FormatType;
  description: string|null;
}
