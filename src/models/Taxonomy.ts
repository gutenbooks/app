import { Taxon } from './Taxon';

export interface Taxonomy {
  id: number;
  name: string;
  root: Taxon;
}
