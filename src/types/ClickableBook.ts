import { Book } from '../models/Book';

export type ClickableBook = (book: Book) => any;
