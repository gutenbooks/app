import { Contributor } from '../models/Contributor';

export type ClickableContributor = (contributor: Contributor) => any;
