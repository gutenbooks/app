import { SearchFormInterface } from '../models/SearchFormInterface';

export type OnSearchCallback = (form: SearchFormInterface) => any;
