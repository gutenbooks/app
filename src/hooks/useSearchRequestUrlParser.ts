import { parse } from 'qs';

import { FormatType } from '../models/FormatType';

export const useSearchRequestUrlParser = () => {
  const {
    query,
    taxons,
    language,
    formats,
    sort,
  } = parse(window.location.search, { ignoreQueryPrefix: true });
  const parsedTaxons = taxons ? taxons as string[] : [];

  let parsedFormats: FormatType[] = [];
  if (formats) {
    if (formats instanceof Array) {
      parsedFormats = formats as FormatType[];
    } else {
      parsedFormats.push(formats as FormatType);
    }
  }

  return {
    query: query as string,
    taxons: parsedTaxons,
    language: language as string || undefined,
    formats: parsedFormats,
    sort,
  };
}
