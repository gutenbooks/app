import { useState, useEffect } from 'react';
import { RequestQueryBuilder } from '@nestjsx/crud-request';

import { OnSearchCallback } from '../types/OnSearchCallback';
import { SearchFormInterface } from '../models/SearchFormInterface';
import { FormatType } from '../models/FormatType';
import { ApiClient } from '../utils/ApiClient';

export interface SearcRequestProps {
  query: string;
  taxons?: string[];
  formats?: FormatType[];
  language?: string;
  sort?: any;
  page?: number;
  searchCallback?: (params: SearchFormInterface) => void;
  queryFilters?: any;
}

export const useSearchRequest = ({
  query,
  taxons = [],
  formats = [],
  language = 'en',
  sort,
  page = 1,
  searchCallback,
  queryFilters,
}: SearcRequestProps) => {
  const [filters] = useState<any>(queryFilters || {});
  const [context, setContext] = useState<SearchFormInterface>({
    page,
    query,
    formats,
    taxons,
    language,
    sort,
  });
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [objects, setObjects] = useState<any[]>([]);
  const [error, setError] = useState<any>();
  const [totalObjects, setTotalObjects] = useState<number>(0);

  useEffect(() => {
    setIsLoading(true);

    const terms = ['title', 'contributions.contributor.name'].map((field) => {
      const term: any = {
        'editions.language.code': context.language,
      };

      // filter=editions.formats.type||$in||html,kindle
      if (context.formats && context.formats.length !== 0) {
        term['editions.formats.type'] = { $in: context.formats };
      }

      if (context.query) {
        term[field] = { $contL: context.query };
      }

      return {
        ...term,
        ...filters,
      };
    });

    const qb = RequestQueryBuilder
      .create()
      .setPage(context.page || 1)
      .search({
        $or: terms
      })
    ;

    if (context.sort) {
      qb.sortBy(context.sort);
    }

    ApiClient.get(`/books/?${qb.query()}`)
      .then(({data}) => {
        setTotalObjects(data.total);

        if (data.data) {
          setObjects((b) => [...b, ...data.data]);
        }

        setIsLoading(false);
      })
      .catch((err) => {
        setError(() => err);
      })
    ;
  }, [context, filters]);

  const onSearch: OnSearchCallback = (form: SearchFormInterface) => {
    let tx: string[] = [];
    if (form.taxons) {
      tx = form.taxons.map((t) => t.toString());
    }

    setObjects([]);
    setContext({
      ...context,
      page: 1,
      query: form.query,
      formats: form.formats || [],
      taxons: tx,
      sort: form.sort,
      language: form.language,
    });

    // update the query params
    const searchParams = form;
    delete searchParams.page;

    if (searchCallback) {
      searchCallback(searchParams);
    }
  };

  const onLoadMore = (e: any) => {
    e.preventDefault();
    setContext({
      ...context,
      page: context.page ? context.page + 1 : 1,
    });
  }

  return {
    onSearch,
    onLoadMore,
    isLoading,
    objects,
    totalObjects,
    context,
    error,
  };
}
