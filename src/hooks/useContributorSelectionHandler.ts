import { useHistory } from 'react-router-dom';

import { Contributor } from '../models/Contributor';
import { ClickableContributor } from '../types/ClickableContributor';

export const useContributorSelectionHandler = (): ClickableContributor => {
  const history = useHistory();
  return (contributor: Contributor) => {
    history.push(`/contributors/${contributor.id}`);
  };
}
