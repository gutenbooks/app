import { useState, useEffect } from 'react';

import { ApiClient } from '../utils/ApiClient';
import { Language } from '../models/Language';

export const useLanguages = (): Language[] => {
  const [languages, setLanguages] = useState<Language[]>([]);

  useEffect(() => {
    ApiClient.get(`/languages/`)
      .then(({data}) => {
        setLanguages(data.data);
      })
    ;
  }, []);

  return languages;
}
