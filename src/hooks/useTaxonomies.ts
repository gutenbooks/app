import { useState, useEffect } from 'react';

import { ApiClient } from '../utils/ApiClient';
import { Taxonomy } from '../models/Taxonomy';

export const useTaxonomies = (): Taxonomy[] => {
  const [taxonomies, setTaxonomies] = useState<Taxonomy[]>([]);

  useEffect(() => {
    ApiClient.get(`/taxonomies/`)
      .then(({data}) => {
        setTaxonomies(data.data);
      })
    ;
  }, []);

  return taxonomies;
}
