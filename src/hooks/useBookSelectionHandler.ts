import { useHistory } from 'react-router-dom';

import { Book } from '../models/Book';
import { ClickableBook } from '../types/ClickableBook';

export const useBookSelectionHandler = (): ClickableBook => {
  const history = useHistory();
  return (book: Book) => {
    history.push(`/books/${book.id}`);
  };
}
