import { useState } from 'react';
import { AxiosInstance } from 'axios';

import { ApiClient } from '../utils/ApiClient';

export const useApiClient = () => {
  const [client] = useState<AxiosInstance>(ApiClient);
  return client;
};
