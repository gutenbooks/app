import { SelectOption } from '../models/SelectOption';

export const FORMAT_OPTIONS: SelectOption[] = [
  { value: 'epub', label: 'EPUB' },
  { value: 'kindle', label: 'Kindle' },
  { value: 'text', label: 'text' },
  { value: 'html', label: 'HTML' },
  { value: 'pdf', label: 'PDF' },
];
