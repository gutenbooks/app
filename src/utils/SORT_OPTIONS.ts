import { SelectOption } from '../models/SelectOption';

export const SORT_OPTIONS: SelectOption[] = [
  {
    value: { field: 'editions.downloads', order: 'DESC' },
    label: 'Most Popular',
  },
  {
    value: { field: 'editions.downloads', order: 'ASC' },
    label: 'Least Popular',
  },
  {
    value: { field: 'title', order: 'ASC' },
    label: 'Title A-Z',
  },
  {
    value: { field: 'title', order: 'DESC' },
    label: 'Title Z-A',
  },
];
