import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';

let baseURL = 'https://api.gutenbooks.com/';
if (process.env.REACT_APP_API_DOMAIN) {
  baseURL = process.env.REACT_APP_API_DOMAIN;
}

// global axios config
const config = {
  baseURL,
  headers: {
    // 'Accepts': 'application/json',
    post: { 'Content-Type': 'application/json' },
    put: { 'Content-Type': 'application/json' },
    patch: { 'Content-Type': 'application/json' },
  },
} as AxiosRequestConfig;

// create a custon axios instance
const ApiClient: AxiosInstance = axios.create(config);

export {
  ApiClient,
}
