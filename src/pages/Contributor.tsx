import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { stringify } from 'qs';

import { useSearchRequest } from '../hooks/useSearchRequest';
import { useSearchRequestUrlParser } from '../hooks/useSearchRequestUrlParser';
import { useContributorSelectionHandler } from '../hooks/useContributorSelectionHandler';
import { useBookSelectionHandler } from '../hooks/useBookSelectionHandler';
import { SearchFormInterface } from '../models/SearchFormInterface';
import { BookItemList } from '../components/Book/BookItemList';
import { ContributorHeader } from '../components/Contributor/ContributorHeader';
import { SearchControl } from '../components/Search/SearchControl';
import { Loader } from '../components/Loader/Loader';
import { Meta } from '../components/Meta';

export const Contributor: React.FC = () => {

  const { id } = useParams();
  const history = useHistory();
  const onClickAuthor = useContributorSelectionHandler();
  const onClickBook = useBookSelectionHandler();
  const onSearchCallback = (params: SearchFormInterface) => {
    history.push({
      pathname: `/contributors/${id}`,
      search: `?${stringify(params)}`,
    });
  };

  const {
    query,
    taxons,
    language,
    formats,
    sort,
  } = useSearchRequestUrlParser();

  const {
    onSearch,
    onLoadMore,
    isLoading,
    objects: books,
    totalObjects: count,
    context,
    // error,
  } = useSearchRequest({
    query,
    taxons,
    formats,
    language,
    sort,
    page: 1,
    searchCallback: onSearchCallback,
    queryFilters: { 'contributions.contributorId': id },
  });

  if (!id) {
    console.log('No contributor specified, redirecting to home.');
    history.push(`/`);
  }

  return (
    <div className="container">
      <Meta/>
      <ContributorHeader id={id as string} />
      <SearchControl
        defaultValues={context}
        onSearch={onSearch}
        placeholder="Search (Title, Subtitle, etc.)"
      />

      <div className="container mt-4">
        <Loader isLoading={isLoading && context.page === 1}>
          <p className="small text-muted">{count} total results</p>
          <BookItemList
            books={books}
            onClickBook={onClickBook}
            onClickAuthor={onClickAuthor}
          />
        </Loader>

        { books.length < count ?
          (
            <div className="text-center my-4">
              <Loader isLoading={isLoading && !!context.page && context.page >= 1}>
                <Button
                  variant="outline-primary"
                  onClick={onLoadMore}
                >
                  Load More
                </Button>
              </Loader>
            </div>
          )
          : null
        }
      </div>
    </div>
  );
};
