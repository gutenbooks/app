import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { stringify } from 'qs';

import { useSearchRequest } from '../hooks/useSearchRequest';
import { useSearchRequestUrlParser } from '../hooks/useSearchRequestUrlParser';
import { useContributorSelectionHandler } from '../hooks/useContributorSelectionHandler';
import { useBookSelectionHandler } from '../hooks/useBookSelectionHandler';
import { SearchFormInterface } from '../models/SearchFormInterface';
import { BookItemList } from '../components/Book/BookItemList';
import { SearchControl } from '../components/Search/SearchControl';
import { Loader } from '../components/Loader/Loader';
import { Meta } from '../components/Meta';

export const Search: React.FC = () => {

  const history = useHistory();
  const onClickAuthor = useContributorSelectionHandler();
  const onClickBook = useBookSelectionHandler();
  const onSearchCallback = (params: SearchFormInterface) => {
    history.push({
      pathname: '/search',
      search: `?${stringify(params)}`,
    });
  };

  const {
    query,
    taxons,
    language,
    formats,
    sort,
  } = useSearchRequestUrlParser();


  const {
    onSearch,
    onLoadMore,
    isLoading,
    objects: books,
    totalObjects: count,
    context,
    // error,
  } = useSearchRequest({
    query,
    taxons,
    language,
    formats,
    sort,
    page: 1,
    searchCallback: onSearchCallback,
  });

  if (!context.query) {
    console.log('No query specified, redirecting to home.');
    history.push(`/`);
  }

  return (
    <div className="container">
      <Meta
        title="Search"
      />
      <SearchControl
        defaultValues={context}
        onSearch={onSearch}
      />

      <div className="container mt-4">
        <Loader isLoading={isLoading && context.page === 1}>
          <p className="small text-muted">{count} total results</p>
          <BookItemList
            books={books}
            onClickBook={onClickBook}
            onClickAuthor={onClickAuthor}
          />
        </Loader>

        { books.length < count ?
          (
            <div className="text-center my-4">
              <Loader isLoading={isLoading && !!context.page && context.page >= 1}>
                <Button
                  variant="outline-primary"
                  onClick={onLoadMore}
                >
                  Load More
                </Button>
              </Loader>
            </div>
          )
          : null
        }
      </div>
    </div>
  );
};
