import React from 'react';
import { useHistory } from 'react-router-dom';
import { stringify } from 'qs';

import { SearchFormInterface } from '../models/SearchFormInterface'
import { SearchControl } from '../components/Search/SearchControl';
import { PopularBooks } from '../components/PopularBooks';
import { RecentBooks } from '../components/RecentBooks';
import { Meta } from '../components/Meta';
import { OnSearchCallback } from '../types/OnSearchCallback';

export const Home: React.FC = () => {
  const history = useHistory();

  const onSearch: OnSearchCallback = (form: SearchFormInterface) => {
    if (form.query) {
      history.push(`/search/?${stringify(form)}`);
    }
  };

  return (
    <div className="container">
      <Meta
        title="Home"
      />
      <SearchControl
        onSearch={onSearch}
      />

      <div className="mt-3">
        <PopularBooks/>
      </div>

      <hr/>

      <div className="mt-3">
        <RecentBooks/>
      </div>
    </div>
  );
};
